from flask import Flask, render_template, url_for

__author__ = 'jrienton'


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')